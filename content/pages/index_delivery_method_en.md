---
page: index
name: delivery
title: Delivery method used
lang: en
---

## Delivery method used

Yemeni Archive also analysed the delivery method by which medical facilities were targeted. The following table summarises these findings.


|Number of incidents|Category|
|--|--|
|71|Airstrikes|
|12|Artillery shelling|
|9|Mortar|
|14|Light weapons|
|2|Rocket launchers|
|6|Missile shelling|
|2|Tank shelling|
|2|Car bomb|
|2|Explosive device|
|13|unknown|

This demonstrates that the majority of medical facilities identified within this database were targeted in airstrikes (71 attacks), followed by artillery shelling (12 attacks) and light weapons (14 attacks). Mortars were used in 9 attacks, and missiles were used in 6 attacks. Tanks, car bombs, rocket launchers, and explosive devices were used all used in 2 attacks each. For 13 attacks on medical facilities, we were unable to determine the delivery method of munition used.


## Repeated attacks

Some medical facilities have been subjected to multiple attacks, and as a result, have gone out of service. Yemeni Archive independently confirmed the repeated targeting of 11 hospitals through using the name of the medical facility, the location, and the date of the attack.

For example, Marib Hospital, Haradh German Hospital, Taiz Military Hospital, Al-Thawra Hospital in Hodeidah, Martyr Saif Al-Sawadi Hospital, the Field Hospital and the Seventy Maternity Hospital were attacked twice. The Yemeni hospital was attacked three times. The May 22 Hospital in Hodeidah was attacked four times, and Hais Hospital was attacked five times. The Al-Thawra Hospital in Taiz was attacked nine times.

| Hospital  Name| Number of Records |
|--|--|
|Al-Thawrah hospital | 9 |
|Hays Al-Rifi hospital | 5 |
|22 Mayo hospital|4|
|Military hospital Taiz|3|
|Yemen international hospital|3|
|Republican hospital Taiz|2|
|Mareb hospital|2|
|Harad German hospital|2|
|Harad general hospital|2|
|Al-Thawrah hospital - Sanaa|2|
|Al-Thawrah hospital - Hudaydah|2|
|Al-Shaheed Saif Al-Sawadi hospital|2|
|Al-Maydani hospital|2|
|Al Sabeen hospital for motherhood|2|
|Al Dourayhmi rural hospital|2|
|Military Hospital Sanaa|2|
