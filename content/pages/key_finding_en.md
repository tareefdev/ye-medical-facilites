---
page: key-finding
name: keyFinding
title: Key Finding
lang: en
---

## Key Findings

All 133 unique incidents (646 observations) of attacks on medical facilities include violations committed against medical facilities and/or other health workers, who are specifically offered protections under International Humanitarian Law. It should be noted that this database may underrepresent the extent to which medical facilities have been targeted - Yemeni Archive has included only incidents for which documentation has been able to be identified and independently verified.


The database contains documentation that shows:

·      The impact of the attacks on medical facilities as a result of airstrikes and rocket attacks by various parties to the conflict.
·      Victims of the airstrikes, including children and women.
·      Remnants of ammunition used in the attacks allegedly by the Saudi-led Arab coalition forces and Houthi forces.
·      Witness testimony of attacks on medical facilities, in which surviving medical staff and civilians testify through video interviews.
·      Rescue operations by humanitarian groups to assist the victims of alleged attacks against medical facilities.

The following figure shows the a time-series analysis of alleged attacks on medical facilities by parties to the conflict in Yemen on a monthly basis:





This analysis demonstrates that since the Saudi-led Coalition began airstrikes, hospitals and medical have been targeted regularly, with a large increase in facilities attacked between March and December 2015. The frequency of attacks was reduced starting in January 2016 and continuing throughout that same year until December. In 2017, airstrikes on medical facilities increased again, peaking between October and December, however the number of airstrikes on medical facilities did not reach the same frequency as observed in 2015. During 2018, airstrikes were most frequent in June and December. In 2019, the number of airstrikes targeting medical facilities was most frequent in in March and August.

Yemeni Archive also analysed the delivery method by which medical facilities were targeted, as well as the munitions used in such attacks. The following table summarises these findings.


Number of incidents
Category
71
Airstrikes
12
Artillery shelling
4
Ground - to - ground missiles
8
Mortar
14
Light weapons
2
Rocket launchers
2
Missile shelling
2
Tank shelling
2
Car bomb
2
Explosive device
14
unknown


These attacks took place in different areas of Yemen. In areas controlled by Houthi forces, 83 medical facilities were targeted; in areas controlled by Yemeni government forces, 17 medical facilities were targeted; in areas controlled by the Saudi-led coalition 31 medical facilities were targeted, and one medical facility was attacked in al-Qaeda controlled area.

The responsibility for the attacks on medical facilities is divided. The Saudi-led coalition forces is allegedly responsible for 71 attacks, Houthis forces are allegedly responsible for 52 attacks, Al-Qaeda is allegedly responsible for 3 attacks, and the forces loyal to the Saudi-led coalition forces are allegedly responsible for 3 attacks.



The 133 attacks identified by Yemeni Archive included four attacks by the Saudi-led coalition forces, directly targeting medical staff and 129 attacks against medical facilities. 16 attacks that killed or assaulted medical staff were by Al Houthis forces. Al Houthis forces were responsible for frequently bombing the Al-Thawra hospital, resulting in the injury of several medical personnel and the partial destruction of the hospital. In Ad Dali city, Al Houthi forces attacked the medical staff inside the hospital. In addition, the Saudi-led coalition forces carried out nine airstrikes that killed medical staff and paramedics in several areas, including Saada, Wahidan and the capital Sanaa.


### Legal Framework

Attacks on medical facilities, medical and health workers and patients are prohibited by international humanitarian law and constitute war crimes. The Geneva Conventions of 1949 and the Additional Protocols of 1977 mandate the protection of health care facilities and health care workers. Medical facilities are protected by virtue of their function but also because they are run by civilians and civilians are likely to be present in large numbers, especially during times of conflict.

The only exception to this rule is when these medical facilities are used for military purposes, for example as weapons storage facilities or military bases from which missiles are launched. Even then, Article 19 of the First Geneva Conventions stipulates that such protection may “cease only after due warning has been given, naming in all appropriate cases a reasonable time limit and after such warning has remained unheeded”.

Security Council 2286 passed in 2016 and co-sponsored by more than 80 UN member states reiterates commitment to international humanitarian law and calls on member states to be proactive in addressing attacks on health care during times of war and in adopting practical measures. The UN Secretary-General has published a list of recommended measures that would help support the implementation of Res.2286. These measures include political, diplomatic and economic pressure on parties found to be perpetrating attacks on health care, refrain from selling arms to perpetrators and investigating attacks and prosecuting violators.


### Victims

we found info of number killed for 59 incidents. Of which, we are able to confirm for 35 incidents

Yemeni Archive has compiled and verified a list of the number of victims from the 35 attacks. Civilians including women, children and medical staff were killed and dozens were injured. While Yemeni Archive could not determine the number of victims from 24 attacks through open-source information due to the conflicting information in numbers about the attacks on Medical facilities. In several attacks, the casualty figures have sometimes reported a total of attacks, including casualties in medical facilities and centers, but also markets and civilian homes. In 74 attacks, no casualties were reported.

The number of victims documented as a result of attacks on medical facilities in Yemen (who were killed by the main actors in the conflict) is 98 people, including three women and 20 children. The number of injured is 191, including eight women and six children as a result of repeated attacks on medical facilities and centers.

Repeated attacks

Some medical facilities have been subjected to multiple attacks, and as a result, have gone out of service. Yemeni Archive independently confirmed the intentional targeting of 11 hospitals by repeated attacks.

Marib Hospital, Haradh German Hospital, Taiz Military Hospital, Al-Thawra Hospital in Hodeidah, Martyr Saif Al-Sawadi Hospital, the Field Hospital and the Seventy Maternity Hospital were attacked twice. While the Yemeni hospital was attacked three times, May 22 Hospital was attacked four times and Hais Hospital was attacked five times. The Al-Thawra Hospital in Taiz was attacked nine times.

### Notable incidents

The Attacks onTargeting the 22 May Governmental Hospital in Hodeidah city

The 22 May Governmental Hospital was targeted three times in 2019, with artillery shells and heavy weapons by the Al Houthis forces. The first attack was on March 5th, the second attack on June 26th and the third attack on August 25th. There are indications that the targeting of the hospital is systematic. According to the analyzed evidence and sources, there were no civilian casualties, but the bombing destroyed and burned the facility.

The Attack onTargeting the Kattaf Rural Hospital

On March 26, 2019, at 9:30 am, the Saudi-led coalition forces, targeted a gas station near the Kattaf Rural Hospital.
