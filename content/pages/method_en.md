---
page: method
name: method
title: Methodology
lang: en
---

## Methodology

This database of attacks on medical facilities was created through archiving and standardising various documentation efforts by citizen journalists, media groups, humanitarian groups, human rights groups and others working in Yemen. The open source database is queryable by date, geographic location, and keyword, and can sorted by relevance, date, or number of videos and reports available for each incident. Maps and data visualisations have been created to demonstrate the scale and severity of attacks. Yemeni Archive will update the database as new documentation becomes available.

The database of attacks on medical facilities draws on open source data as well as data compiled by the UN Group of Eminent Experts and human rights monitoring organisations including Physicians for Human Rights, Médecins Sans Frontières, Yemen Data Project, the Yemeni Coalition for Monitoring Human Rights Violations, Mwatana, and many more researchers and reports from a number of Yemeni and international civil society organisations.

The Yemeni Archive team examined a digital database with more than 1755 observations that document attacks on hospitals, health centers and medical staff by various parties to the conflict. This includes 596 Facebook, 526 Twitter, 159 Youtube and 474 websites. The videos and linked digital sources have been verified according to the research methodology for digital evidence at Yemeni Archive. Metadata was extracted and standardised using a standardised data ontology. Through this process, new incidents have been identified that were not included in any of the published human rights reports by various organisations.

Yemeni Archive thoroughly researched each of the 133 incidents. This required extracting data observations from our archival infrastructure on specific incidents, based on dates and geographic location, as well as using content discovery tools to search on social media platforms like whopostedwhat.com / Twitter advanced search and Google advanced search.

Yemeni Archive populated the database by adding a summary of the incident after reviewing and verifying the digital content (video, photos and information posted to social media sites), creating a title for the incident and locating the attack as well as alleged perpetrators. Through this process, Yemeni Archive has transformed the data into useful, searchable information.

All content used to build this database is open source, extracted, processed and publicly published.

Although the range of incidents documented is comprehensive in scope, the availability of data varies and so while some incidents have been extensively documented, there are gaps in information for others. Yemeni Archive is committed to continue monitoring attacks on medical facilities and updating the database as data becomes available.


## The Ethical Framework of Data Collection

Yemeni Archive strives to implement a harm reduction ethical framework as well as transparency with regard to our methodology and findings. We believe that visual documentation of human rights violations preserved and verified in a transparent, detailed and reliable manner is a key step towards achieving accountability and positively contributing to post-conflict peace and stability. Such content can humanise victims, reduce disagreement over death tolls, help communities realise the real human cost of war, and support efforts towards truth and reconciliation.

To that end, we have sought to be detailed and transparent in the presentation process, taking into account the need to protect those presenting these relevant documents. Taking these interests seriously, some of the detailed information methodologies that would deem sensitive has not been withheld to protect the security of our sources.

For example, our team knows that geolocation is necessary to make claims and verify content. In many cases, Yemeni Archive publishes the geo-coordinates of alleged attack sites. However, due to repeated targeting of hospitals and medical facilities, we have determined that publicly posting the accurate geographic locations of facilities could pose additional potential risks for those working in such environments, even for facilities that have gone out of the service. Therefore, two versions of this report have been prepared: a public version with concise results, and a special version containing additional information, such as coordinates. This special edition is presented to the Office of the High Commissioner for Human Rights (OHCHR) and to the Group of Eminent Experts on Yemen (GEE), as well as human rights organisations such as Human Rights Watch and Amnesty International.


## Tools

The team of Yemeni Archive used a special methodology to investigate the stories in this report, the Whopostedwhat tool was used to gather more accurate information about the nature and timing of the attack by searching for the first published information about the attack and then analysing the timestamp of Facebook posts using the 4webhelp tool. In order to verify the attacks, Yemeni Archive team searched historic Google Earth Pro images, analysed the visual documentation associated with the attack and verified its compatibility with the satellite images. These images were also compared with the visual content of each attack and identified by visible and prominent places in photos and in linked videos.

Yemeni Archive also relied on EZsearch to identify reports and videos linked to each attack on YouTube in the days following the incident in order to make the search more accurate. Yemeni Archive also relied on a field research team, in-person interviews with health facility workers and eyewitness interviews, and the Yemen Data Project to document the numbers of victims of attacks.


## Acknowledgements

The videos in this database have been collected by many groups and individuals in Yemen. Currently in this database there are 1755 different pieces of data This collection would not be possible without the brave work of those documenting and reporting on war crimes and crimes against humanity. Our thanks goes to each and every one of them.

Our team would like to thank all those who helped in verifying, annotating and analysing this data. In particular, we would like to thank the student groups at Taiz University for their work verifying and annotating open source content and contributing to investigations. In particular we would like to thank Abdullah Al Mamari and Olfat A. We would also like to thank Mansour Omari, Lilas Al Boni, Sawsan Abdul-Jalil, Obieda Falhha, and Abeer Mohsen.

This project was made possible by International Media Support (IMS), Check Global - MENA IF  through their grant to support students trained by Yemeni Archive.
