import colorMapper from "./color-mapper";

const formatJsonData = jsonData => {
  /*
      SAMPLE JSON FORMAT:
      {
        children: [
          {
            name: "Pro- Coalition forces",
            children: [
              {
                "code": "Code",
                "hospital": "name of the hospital_en",
                "location": "location . Analysis"
              }
            ]
          }
        ]
      }
      */
  const children = [];
  const perpetrators = jsonData
    .map(dataObject => {
      return dataObject["alleged_perpetrator"];
    })
    .filter(perpetrator => {
      return perpetrator !== "UNKNOWN" && perpetrator;
    });
  const uniquePerps = perpetrators.reduce(
    (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
    []
  );
  uniquePerps.forEach(perpetrator => {
    const perpsFormatted = jsonData
      .map(dataPoint => {
        if (dataPoint["alleged_perpetrator"] === perpetrator) {
          const formattedData = {
            code: dataPoint["id"],
            name: dataPoint["hospital_name_en"],
            location: dataPoint["location"],
            perpetrator: dataPoint["alleged_perpetrator"],
            value: 7,
            color: colorMapper[perpetrator]
            //name: dataPoint["annotations.title_en"]
          };
          return formattedData;
        }
      })
      .filter(dataPoint => {
        return dataPoint !== undefined;
      });

    children.push({
      name: perpetrator,
      children: perpsFormatted
    });
  });
  const finalData = {
    name: "Perpetrators",
    children: children
  };
  return finalData;
};
export default formatJsonData;
